﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;

namespace HealthDemo
{
    public partial class _Default : System.Web.UI.Page
    {

        #region PAGE PROPERTIES
        private int SessINT(string n)
        {
            if (Session[n] != null)
            {
                return CommonCode.strInt(Session[n].ToString());
            }
            else
            {
                return -1;
            }
        }
        private Nullable<int> SessINTnull(string n)
        {
            if (Session[n] != null)
            {
                string bID = Session[n].ToString();
                return CommonCode.strInt(bID);
            }
            else
                return null;
        }
        public int ACCID { get { return SessINT("accID"); } set { Session["accID"] = value; } }
        public string HDPROFILE
        {
            get { return CommonCode.BlankIfNull(Session["HDProfile"]); }
            set { Session["HDProfile"] = CommonCode.NullIfBlank(value); }
        }
        public string HDSTATE
        {
            get { return CommonCode.BlankIfNull(Session["HDState"]); }
            set { Session["HDState"] = CommonCode.NullIfBlank(value); }
        }
        public string HDTICKTHIS
        {
            get { return CommonCode.BlankIfNull(Session["HDTickThis"]); }
            set { Session["HDTickThis"] = CommonCode.NullIfBlank(value); }
        }
        public string HDCURRENTSORT
        {
            get { return CommonCode.BlankIfNull(Session["HDCurrentSort"]); }
            set { Session["HDCurrentSort"] = CommonCode.NullIfBlank(value); }
        }
        public string HDLASTORDERBY
        {
            get { return CommonCode.BlankIfNull(Session["HDLastOrderBy"]); }
            set { Session["HDLastOrderBy"] = CommonCode.NullIfBlank(value); }
        }
        public string HDALERT
        {
            get { return CommonCode.BlankIfNull(Session["HDAlert"]); }
            set { Session["HDAlert"] = CommonCode.NullIfBlank(value); }
        }
        public string HDEXPANDLOG
        {
            get { return CommonCode.BlankIfNull(Session["HDExpandLog"]); }
            set { Session["HDExpandLog"] = CommonCode.NullIfBlank(value); }
        }
        public string HDNEWMESSAGES
        {
            get { return CommonCode.BlankIfNull(Session["HDNewMessages"]); }
            set { Session["HDNewMessages"] = CommonCode.NullIfBlank(value); }
        }
        public string HDAPIKEY
        {
            get
            {
                string m_result = CommonCode.BlankIfNull(Session["HDApiKey"]);
                if (m_result == "")
                {
                    DataTable dt = ExecuteSql("select refText from tblReference where refGroupId=3 and refKey='API Key'");
                    if (dt != null)
                        if (dt.Rows.Count > 0)
                        {
                            m_result = dt.Rows[0][0].ToString();
                            Session["HDApiKey"] = m_result;
                        }
                }
                return m_result;
            }
        }
        public string HDSECRETKEY
        {
            get
            {
                string m_result = CommonCode.BlankIfNull(Session["HDSecretKey"]);
                if (m_result == "")
                {
                    DataTable dt = ExecuteSql("select refText from tblReference where refGroupId=3 and refKey='Secret Key'");
                    if (dt != null)
                        if (dt.Rows.Count > 0)
                        {
                            m_result = dt.Rows[0][0].ToString();
                            Session["HDSecretKey"] = m_result;
                        }
                }
                return m_result;
            }
        }

        #endregion

        #region Render
        protected override void Render(HtmlTextWriter writer)
        {
            if (panel_AMSReceive.Visible)
            {
                foreach (GridViewRow row in gv_receivedMessages.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        ClientScript.RegisterForEventValidation(gv_receivedMessages.UniqueID, "Select$" + row.RowIndex.ToString());
                    }
                }
            }
            if (panel_allLogs.Visible)
            {
                foreach (GridViewRow row in gv_queryLogs.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        ClientScript.RegisterForEventValidation(gv_queryLogs.UniqueID, "Select$" + row.RowIndex.ToString());
                    }
                }
                foreach (GridViewRow row in gv_messageLogs.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        ClientScript.RegisterForEventValidation(gv_messageLogs.UniqueID, "Select$" + row.RowIndex.ToString());
                    }
                }
            }
            if (gv_allAppointments.Controls.Count > 0 && panel_AllAppointments.Visible)
            {
                string[] sortData = HDCURRENTSORT.Split('\t');
                Table tblGrid = gv_allAppointments.Controls[0] as Table;
                DateTime lastDay = new DateTime(1900, 1, 1);
                string lastPatient = "";
                string lastType = "";
                foreach (GridViewRow row in gv_allAppointments.Rows)
                {
                    if (row.Visible && (sortData[0] == "Sort_Dttm" || sortData[0] == "Sort_Patient" || sortData[0] == "Sort_Site" || sortData[0] == "Sort_Location"))
                    {
                        int index = tblGrid.Rows.GetRowIndex(row);
                        string heading = "";
                        if (sortData[0] == "Sort_Dttm")
                        {
                            DateTime thisDate;
                            if (row.Cells[1].Text == "" || row.Cells[1].Text == "&nbsp;")
                            {
                                if (lastDay != new DateTime(1970, 1, 1))
                                {
                                    heading = "No Return Date";
                                    lastDay = new DateTime(1970, 1, 1);
                                }
                            }
                            else
                            {
                                if (DateTime.TryParse(row.Cells[1].Text, out thisDate))
                                {
                                    thisDate = new DateTime(thisDate.Year, thisDate.Month, thisDate.Day);
                                    if (thisDate != lastDay)
                                    {
                                        heading = thisDate.ToString("dddd d, MMMM yyyy");
                                        lastDay = thisDate;
                                    }
                                }
                            }
                        }
                        else if (sortData[0] == "Sort_Patient")
                        {
                            if (row.Cells[2].Text != lastPatient)
                                heading = lastPatient = row.Cells[2].Text;
                        }
                        else if (sortData[0] == "Sort_Site")
                        {
                            if (row.Cells[3].Text == "" || row.Cells[3].Text == "&nbsp;")
                            {
                                if (row.Cells[3].Text != lastType)
                                    heading = lastType = "Site not specified";
                            }
                            else
                            {
                                if (row.Cells[3].Text != lastType)
                                    heading = lastType = row.Cells[3].Text;
                            }
                        }
                        else if (sortData[0] == "Sort_Location")
                        {
                            if (row.Cells[4].Text == "" || row.Cells[4].Text == "&nbsp;")
                            {
                                if (row.Cells[4].Text != lastType)
                                    heading = lastType = "Location not specified";
                            }
                            else
                            {
                                if (row.Cells[4].Text != lastType)
                                    heading = lastType = row.Cells[4].Text;
                            }
                        }
                        if (heading != "")
                        {
                            GridViewRow rowHead = new GridViewRow(index, index, DataControlRowType.Separator, DataControlRowState.Normal);
                            TableCell newCell = new TableCell();
                            newCell.Text = heading;
                            newCell.CssClass = "HDSortHead";
                            newCell.ColumnSpan = gv_allAppointments.Columns.Count;
                            rowHead.Cells.Add(newCell);
                            tblGrid.Controls.AddAt(index, rowHead);
                        }
                    }
                    ClientScript.RegisterForEventValidation(gv_allAppointments.UniqueID, "Select$" + row.RowIndex.ToString());
                }
                if (sortData[0] == "Sort_Dttm")
                    gv_allAppointments.HeaderRow.Cells[1].BackColor = System.Drawing.Color.Black;
                else if (sortData[0] == "Sort_Patient")
                    gv_allAppointments.HeaderRow.Cells[2].BackColor = System.Drawing.Color.Black;
                else if (sortData[0] == "Sort_Site")
                    gv_allAppointments.HeaderRow.Cells[3].BackColor = System.Drawing.Color.Black;
                else if (sortData[0] == "Sort_Location")
                    gv_allAppointments.HeaderRow.Cells[4].BackColor = System.Drawing.Color.Black;
            }
            base.Render(writer);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (HDALERT != "")
                {
                    pageLoadAlert.Value = HDALERT.ToString();
                    String scriptString = "<script language=JavaScript> $(document).ready(function () {\n";
                    scriptString += "var v = document.getElementById('" + pageLoadAlert.ClientID.ToString() + "');\n";
                    scriptString += "if (v.value!='') alert(v.value);";
                    scriptString += "});</script>";

                    if (!ClientScript.IsStartupScriptRegistered("Startup"))
                        ClientScript.RegisterStartupScript(this.GetType(), "Startup", scriptString);
                    HDALERT = "";
                }
                if (Request.QueryString["level"] != "")
                {
                    int level = 0;
                    if (Int32.TryParse(Request.QueryString["level"], out level))
                        HDSTATE = CommonCode.StateSetLevel(HDSTATE, level);
                }
                ShowAppropriatePanels();
            }
        }

        private void ShowAppropriatePanels()
        {
            if (HDSTATE == "")
            {
                InitVariables();
                panel_patients.Visible = panel_receive.Visible = panel_allLogs.Visible = false;
                panel_AllAppointments.Visible = panel_AdminButtons.Visible = true;
                DataTable dt = DT_AllAppointments("apptDttm asc");
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        row_apptAction.Visible = row_apptNone.Visible = false;
                        row_apptGridView.Visible = true;
                        HDCURRENTSORT = "Sort_Dttm\tAsc";
                        gv_allAppointments.DataSource = dt;
                        gv_allAppointments.DataBind();
                        HDLASTORDERBY = "apptDttm asc";
                    }
                    else
                    {
                        row_apptGridView.Visible = row_apptAction.Visible = false;
                        row_apptNone.Visible = true;
                    }
                }
                else
                {
                    row_apptGridView.Visible = row_apptAction.Visible = false;
                    row_apptNone.Visible = true;
                }
            }
            else if (HDSTATE.Split('\t')[0] == "Admin")
            {
                panel_patients.Visible = true;
                panel_receive.Visible = panel_AllAppointments.Visible = panel_AdminButtons.Visible = panel_allLogs.Visible = false;
                ddl_patient.DataSource = DT_Patients();
                ddl_patient.DataValueField = "ptMrn";
                ddl_patient.DataTextField = "Patient";
                ddl_patient.DataBind();
                ddl_patient.SelectedIndex = 0;
                if (ddl_patient.SelectedValue == "")
                    EditPatient("");
                else
                    ShowAppointments(ddl_patient.SelectedValue);
            }
            else if (HDSTATE.Split('\t')[0] == "Receive")
            {
                panel_receive.Visible = true;
                panel_patients.Visible = panel_AllAppointments.Visible = panel_AdminButtons.Visible = panel_allLogs.Visible = false;
                lbl_receiveNoMessages.Visible = gv_receivedMessages.Visible = false;
                DataTable dt = DT_GetMessages(HDNEWMESSAGES = ReceiveAMS());
                if (dt != null) {
                    if (dt.Rows.Count > 0) {
                        gv_receivedMessages.Visible = true;
                        gv_receivedMessages.DataSource = dt;
                        gv_receivedMessages.DataBind();
                    } else {
                        lbl_receiveNoMessages.Visible = true;
                    }
                } else {
                    lbl_receiveNoMessages.Visible = true;
                }
            }
            else if (HDSTATE.Split('\t')[0] == "Logs")
            {
                panel_allLogs.Visible = true;
                panel_patients.Visible = panel_AllAppointments.Visible = panel_AdminButtons.Visible = panel_receive.Visible = false;
                lbl_receiveNoMessages.Visible = gv_receivedMessages.Visible = false;
                gv_queryLogs.DataSource = ExecuteSql("exec rp_getQueryLog");
                gv_queryLogs.DataBind();
                gv_messageLogs.DataSource = DT_GetMessages("All");
                gv_messageLogs.DataBind();
            }
        }

        private void InitVariables()
        {
            HDEXPANDLOG = HDNEWMESSAGES = null;
        }

        protected void gv_allAppointments_OnSorting(object sender, GridViewSortEventArgs e)
        {
            string orderBy = "";
            string direction = SetDirection(e.SortExpression);
            if (e.SortExpression == "Sort_Dttm")
            {
                orderBy = "apptDttm " + direction + ",Patient " + direction;
            }
            else if (e.SortExpression == "Sort_Patient")
            {
                orderBy = "Patient " + direction + ",apptDttm " + direction;
            }
            else if (e.SortExpression == "Sort_Site")
            {
                orderBy = "apptSite " + direction + ",apptDttm " + direction;
            }
            else if (e.SortExpression == "Sort_Location")
            {
                orderBy = "apptLocation " + direction + ",apptDttm " + direction;
            }
            gv_allAppointments.DataSource = DT_AllAppointments(orderBy);
            gv_allAppointments.DataBind();
            HDLASTORDERBY = orderBy;
        }

        private string SetDirection(string sortExpression)
        {
            string newSortDirection = String.Empty;
            if (HDCURRENTSORT == "")
            {
                HDCURRENTSORT = "Sort_Dttm\tAsc";
                newSortDirection = "Asc";
            }
            else
            {
                string[] current = HDCURRENTSORT.Split('\t');
                if (current[0] == sortExpression)
                    newSortDirection = (CommonCode.GetStringElement(current, 1) == "Desc") ? "Asc" : "Desc";
                else
                    newSortDirection = "Asc";
                HDCURRENTSORT = sortExpression + "\t" + newSortDirection;
            }
            return newSortDirection;
        }

        protected void lnkbtn_PatientAdmin_Click(object sender, EventArgs e)
        {
            HDSTATE = "Admin\t\tPatients and Appointments";
            Response.Redirect("Default.aspx", false);
            return;
        }

        protected void lnkbtn_ReceiveMessages_Click(object sender, EventArgs e)
        {
            HDSTATE = "Receive\t\tReceive Messages";
            Response.Redirect("Default.aspx", false);
            return;
        }

        protected void lnkbtn_AllLogs_Click(object sender, EventArgs e)
        {
            HDSTATE = "Logs\t\tView Logs";
            Response.Redirect("Default.aspx", false);
            return;
        }

        protected void gv_queryLogs_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (HDEXPANDLOG == GetHiddenInt(e.Row, "hdn_logId").ToString())
                {
                    e.Row.FindControl("lbl_logIn").Visible = e.Row.FindControl("lbl_logOut").Visible = false;
                    e.Row.FindControl("pnl_logIn").Visible = e.Row.FindControl("pnl_logOut").Visible = true;
                    (e.Row.FindControl("lit_viewLogIn") as Literal).Text = Server.HtmlEncode(FormatXml(GetHiddenString(e.Row, "hdn_logIn")));
                    (e.Row.FindControl("lit_viewLogOut") as Literal).Text = Server.HtmlEncode(FormatXml(GetHiddenString(e.Row, "hdn_logOut")));
                }
                else
                {
                    (e.Row.FindControl("lbl_logIn") as Label).Text = Server.HtmlEncode(GetHiddenString(e.Row, "hdn_shortIn"));
                    (e.Row.FindControl("lbl_logOut") as Label).Text = Server.HtmlEncode(GetHiddenString(e.Row, "hdn_shortOut"));
                }
                e.Row.Attributes.Add("onclick", ClientScript.GetPostBackEventReference(gv_queryLogs, "Select$" + e.Row.RowIndex.ToString()));
            }
        }

        protected void gv_queryLogs_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow row = gv_queryLogs.SelectedRow;
            if (row != null)
            {
                HDEXPANDLOG = GetHiddenInt(row, "hdn_logId").ToString();
                gv_queryLogs.DataSource = ExecuteSql("exec rp_getQueryLog");
                gv_queryLogs.DataBind();
            }
        }

        protected void gv_messageLogs_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (HDEXPANDLOG == GetHiddenString(e.Row, "hdn_msgId"))
                {
                    e.Row.FindControl("lbl_msgContent").Visible = false;
                    e.Row.FindControl("pnl_msgContent").Visible = true;
                    (e.Row.FindControl("lit_msgContent") as Literal).Text = Server.HtmlEncode(FormatXml(FormatXml("<amsMessage id=\"" + (e.Row.FindControl("lbl_messageId") as Label).Text + "\">" + (e.Row.FindControl("hdn_msgContent") as HiddenField).Value + "</amsMessage>")));
                }
                else
                {
                    (e.Row.FindControl("lbl_msgContent") as Label).Text = Server.HtmlEncode(GetHiddenString(e.Row, "hdn_ShortContent"));
                }
                e.Row.Attributes.Add("onclick", ClientScript.GetPostBackEventReference(gv_messageLogs, "Select$" + e.Row.RowIndex.ToString()));
            }
        }

        protected void gv_messageLogs_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow row = gv_messageLogs.SelectedRow;
            if (row != null)
            {
                HDEXPANDLOG = GetHiddenString(row, "hdn_msgId");
                gv_messageLogs.DataSource = DT_GetMessages("All");
                gv_messageLogs.DataBind();
            }
        }

        protected void chkSelect_CheckedChanged(object sender, EventArgs e)
        {
            foreach (GridViewRow row in gv_allAppointments.Rows)
            {
                if ((row.FindControl("chkSelect") as CheckBox).Checked)
                {
                    row_apptAction.Visible = true;
                    return;
                }
            }
            row_apptAction.Visible = false;
        }

        protected void lnkBtn_Send_Click(object sender, EventArgs e)
        {
            row_apptAction.Visible = false;
            panel_AMSSend.Visible = true;
            ddl_TemplateId.DataSource = ExecuteSql("select refKey,refText from tblReference where refGroupId=1 order by refSortOrder");
            ddl_TemplateId.DataValueField = "refKey";
            ddl_TemplateId.DataTextField = "refText";
            ddl_TemplateId.DataBind();
            ddl_TemplateId.SelectedIndex = 0;
            ddl_Destination.DataSource = ExecuteSql("select refKey,refText from tblReference where refGroupId=2 order by refSortOrder");
            ddl_Destination.DataValueField = "refKey";
            ddl_Destination.DataTextField = "refText";
            ddl_Destination.DataBind();
            ddl_Destination.SelectedIndex = 0;
            foreach (GridViewRow row in gv_allAppointments.Rows)
                (row.FindControl("chkSelect") as CheckBox).Enabled = false;
        }

        protected void lnkBtn_transmit_Click(object sender, EventArgs e)
        {
            string messages = "";
            foreach (GridViewRow row in gv_allAppointments.Rows)
            {
                if ((row.FindControl("chkSelect") as CheckBox).Checked)
                {
                    if (messages != "")
                        messages += "\n";
                    messages += "<appearance>" + MakeXml(row, "hdn_apptMrn", "mRN") + MakeXml(row, "hdn_ptFamilyName", "familyName") + MakeXml(row, "hdn_ptGivenName", "givenNames") +
                                                MakeXml(row, "hdn_ptGender", "sex") + MakeDateTimeXml(row, "hdn_ptDOB", "dateOfBirth") + MakeXml(row, "hdn_address", "address") +
                                                MakeXmlFromCell(row, 3, "site") + MakeXmlFromCell(row, 4, "locationOfAppt") + MakeDateTimeXmlFromCell(row, 1, "dateTimeAppt") + "</appearance>";
                }
            }
            if (messages != "")
            {
                SendAMS(ddl_TemplateId.SelectedValue, ddl_Destination.SelectedValue, messages.Split('\n'));
                Response.Redirect("Default.aspx", false);
                return;
                //panel_AMSSend.Visible = false;
                //foreach (GridViewRow row in gv_allAppointments.Rows)
                //{
                //    CheckBox chk = row.FindControl("chkSelect") as CheckBox;
                //    chk.Checked = false;
                //    chk.Enabled = true;
                //}
            }
        }

        private string MakeXml(GridViewRow row, string hiddenField, string xmlTag)
        {
            string value = GetHiddenString(row, hiddenField);
            if (value == "")
                return "<" + xmlTag + "/>";
            else
                return "<" + xmlTag + ">" + value + "</" + xmlTag + ">";
        }

        private string MakeXmlFromCell(GridViewRow row, int cell, string xmlTag)
        {
            string value = row.Cells[cell].Text;
            if (value == "")
                return "<" + xmlTag + "/>";
            else
                return "<" + xmlTag + ">" + value + "</" + xmlTag + ">";
        }

        private string MakeDateTimeXml(GridViewRow row, string hiddenField, string xmlTag)
        {
            string value = GetHiddenString(row, hiddenField);
            if (value != "")
            {
                DateTime thisDt;
                if (DateTime.TryParse(value, out thisDt))
                {
                    return "<" + xmlTag + ">" + thisDt.ToUniversalTime().ToString("yyyy-MM-dd'T'HH:mm:ssZ") + "</" + xmlTag + ">";
                }
            }
            return "<" + xmlTag + "/>";
       }

        private string MakeDateTimeXmlFromCell(GridViewRow row, int cell, string xmlTag)
        {
            string value = row.Cells[cell].Text;
            if (value != "")
            {
                DateTime thisDt;
                if (DateTime.TryParse(value, out thisDt))
                {
                    return "<" + xmlTag + ">" + thisDt.ToUniversalTime().ToString("yyyy-MM-dd'T'HH:mm:ssZ") + "</" + xmlTag + ">";
                }
            }
            return "<" + xmlTag + "/>";
        }

        #region PATIENTS
        private void EditPatient(string mrn)
        {
            if (mrn == "")
            {
                txt_ptMrn.Text = txt_ptFamilyName.Text = txt_ptGivenName.Text = txt_ptDOB.Text = "";
                txt_ptAddressLine1.Text = txt_ptAddressLine2.Text = txt_ptSuburb.Text = txt_ptPostCode.Text = "";
                lnkBtn_CancelPatient.Visible = false;
                lnkBtn_EditPatient.Text = lnkBtn_EditPatient.Text.Replace("Edit", "Add").Replace("Update", "Add");
            }
            else
            {
                DataTable pt = ExecuteSql("select * from tblPatient where ptMrn='" + mrn.Replace("'", "''") + "'");
                if (pt != null)
                {
                    if (pt.Rows.Count > 0)
                    {
                        txt_ptMrn.Text = pt.Rows[0]["ptMrn"].ToString();
                        txt_ptFamilyName.Text = pt.Rows[0]["ptFamilyName"].ToString();
                        txt_ptGivenName.Text = pt.Rows[0]["ptGivenName"].ToString();
                        if (pt.Rows[0]["ptDOB"].GetType().Name == "DBNull")
                            txt_ptDOB.Text = "";
                        else
                            txt_ptDOB.Text = ((DateTime)pt.Rows[0]["ptDOB"]).ToString("d/MMM/yyyy");
                        ddl_ptGender.SelectedValue = pt.Rows[0]["ptGender"].ToString();
                        txt_ptAddressLine1.Text = pt.Rows[0]["ptAddressLine1"].ToString();
                        txt_ptAddressLine2.Text = pt.Rows[0]["ptAddressLine2"].ToString();
                        txt_ptSuburb.Text = pt.Rows[0]["ptSuburb"].ToString();
                        txt_ptPostCode.Text = pt.Rows[0]["ptPostCode"].ToString();
                        lnkBtn_EditPatient.Text = lnkBtn_EditPatient.Text.Replace("Edit", "Update");
                        lnkBtn_CancelPatient.Visible = true;
                    }
                }
            }
            txt_ptMrn.BackColor = txt_ptFamilyName.BackColor = System.Drawing.Color.White;
            row_PatientMrn.Visible = row_PatientFamily.Visible = row_PatientGiven.Visible = row_PatientDOB.Visible = row_PatientGender.Visible = true;
            row_PatientAddressLine1.Visible = row_PatientAddressLine2.Visible = row_PatientSuburb.Visible = row_PatientPostCode.Visible = true;
            Page.SetFocus(txt_ptMrn);
            ShowAppointments("");
        }

        protected void ddl_patient_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddl_patient.SelectedValue == "")
            {
                EditPatient("");
            }
            else
            {
                row_PatientMrn.Visible = row_PatientFamily.Visible = row_PatientGiven.Visible = row_PatientDOB.Visible = row_PatientGender.Visible = false;
                row_PatientAddressLine1.Visible = row_PatientAddressLine2.Visible = row_PatientSuburb.Visible = row_PatientPostCode.Visible = false;
                lnkBtn_CancelPatient.Visible = false;
                lnkBtn_EditPatient.Text = lnkBtn_EditPatient.Text.Replace("Add", "Edit").Replace("Update", "Edit");
                ShowAppointments(ddl_patient.SelectedValue);
            }
        }

        protected void lnkBtn_EditPatient_Click(object sender, EventArgs e)
        {
            if (lnkBtn_EditPatient.Text.Contains("Add"))
            {
                if (ValidatePatient())
                {
                    if (ChangePatient(1, txt_ptMrn.Text.Trim(), txt_ptFamilyName.Text.Trim(), txt_ptGivenName.Text.Trim(), txt_ptDOB.Text.Trim(), ddl_ptGender.SelectedValue,
                                                txt_ptAddressLine1.Text.Trim(), txt_ptAddressLine2.Text.Trim(), txt_ptSuburb.Text.Trim(), txt_ptPostCode.Text.Trim()))
                    {
                        ddl_patient.DataSource = DT_Patients();
                        ddl_patient.DataValueField = "ptMrn";
                        ddl_patient.DataTextField = "Patient";
                        ddl_patient.DataBind();
                        ddl_patient.SelectedValue = txt_ptMrn.Text.Trim();
                        ddl_patient_SelectedIndexChanged(ddl_patient, e);
                    }
                    else
                    {
                        txt_ptMrn.BackColor = System.Drawing.Color.Yellow;
                        Page.SetFocus(txt_ptMrn);
                    }
                }
            }
            else if (lnkBtn_EditPatient.Text.Contains("Edit"))
            {
                EditPatient(ddl_patient.SelectedValue);
            }
            else if (lnkBtn_EditPatient.Text.Contains("Update"))
            {
                if (ValidatePatient())
                {
                    if (ChangePatient(0, txt_ptMrn.Text.Trim(), txt_ptFamilyName.Text.Trim(), txt_ptGivenName.Text.Trim(), txt_ptDOB.Text.Trim(), ddl_ptGender.SelectedValue,
                                                txt_ptAddressLine1.Text.Trim(), txt_ptAddressLine2.Text.Trim(), txt_ptSuburb.Text.Trim(), txt_ptPostCode.Text.Trim()))
                    {
                        row_PatientMrn.Visible = row_PatientFamily.Visible = row_PatientGiven.Visible = row_PatientDOB.Visible = row_PatientGender.Visible = false;
                        row_PatientAddressLine1.Visible = row_PatientAddressLine2.Visible = row_PatientSuburb.Visible = row_PatientPostCode.Visible = false;
                        lnkBtn_CancelPatient.Visible = false;
                        lnkBtn_EditPatient.Text = lnkBtn_EditPatient.Text.Replace("Update", "Edit");
                        ddl_patient.DataSource = DT_Patients();
                        ddl_patient.DataValueField = "ptMrn";
                        ddl_patient.DataTextField = "Patient";
                        ddl_patient.DataBind();
                        ShowAppointments(ddl_patient.SelectedValue = txt_ptMrn.Text.Trim());
                    }
                    else
                    {
                        txt_ptMrn.BackColor = System.Drawing.Color.Yellow;
                        Page.SetFocus(txt_ptMrn);
                    }
                }
            }
        }

        protected void lnkBtn_CancelPatient_Click(object sender, EventArgs e)
        {
            row_PatientMrn.Visible = row_PatientFamily.Visible = row_PatientGiven.Visible = row_PatientDOB.Visible = row_PatientGender.Visible = false;
            row_PatientAddressLine1.Visible = row_PatientAddressLine2.Visible = row_PatientSuburb.Visible = row_PatientPostCode.Visible = false;
            lnkBtn_CancelPatient.Visible = false;
            lnkBtn_EditPatient.Text = lnkBtn_EditPatient.Text.Replace("Update", "Edit");
            ShowAppointments(ddl_patient.SelectedValue);
        }

        private bool ValidatePatient()
        {
            bool bUpdate = true;
            if (txt_ptMrn.Text.Trim() == "")
            {
                txt_ptMrn.BackColor = System.Drawing.Color.Yellow;
                Page.SetFocus(txt_ptMrn);
                bUpdate = false;
            }
            else
                txt_ptMrn.BackColor = System.Drawing.Color.White;
            if (txt_ptFamilyName.Text.Trim() == "")
            {
                txt_ptFamilyName.BackColor = System.Drawing.Color.Yellow;
                if (bUpdate)
                {
                    Page.SetFocus(txt_ptFamilyName);
                    bUpdate = false;
                }
            }
            else
                txt_ptFamilyName.BackColor = System.Drawing.Color.White;
            return bUpdate;
        }
        #endregion

        #region APPOINTMENTS
        private void ShowAppointments(string mrn)
        {
            if (panel_Appointments.Visible = (mrn != ""))
            {
                gv_appointments.DataSource = DT_Appointments(mrn);
                gv_appointments.DataBind();
            }
        }

        protected void gv_appointments_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int apptId = GetHiddenInt(e.Row, "hdn_apptId");
                DateTime apptDttm;
                if (DateTime.TryParse(GetHiddenString(e.Row, "hdn_apptDttm"), out apptDttm))
                {
                    (e.Row.FindControl("ddl_apptHours") as DropDownList).SelectedValue = apptDttm.ToString("HH");
                    (e.Row.FindControl("ddl_apptMins") as DropDownList).SelectedValue = apptDttm.ToString("mm");
                }
                (e.Row.FindControl("btn_apptAdd") as Button).Visible = (apptId == 0);
                (e.Row.FindControl("btn_apptDelete") as Button).Visible = (apptId > 0);
                (e.Row.FindControl("btn_apptUpdate") as Button).Visible = (apptId > 0);
                (e.Row.FindControl("btn_apptTick") as Button).Visible = (apptId.ToString() == HDTICKTHIS);
                e.Row.BackColor = (apptId == 0 ? System.Drawing.Color.LightBlue : System.Drawing.Color.Transparent);
            }
        }

        protected void btn_apptAdd_Command(object sender, CommandEventArgs e)
        {
            int iRow = 0;
            if (Int32.TryParse((sender as Button).CommandArgument, out iRow))
            {
                GridViewRow row = gv_appointments.Rows[iRow];
                TextBox dttm = row.FindControl("txtDttm") as TextBox;
                if (dttm.Text.Trim() == "")
                {
                    dttm.BackColor = System.Drawing.Color.Yellow;
                    Page.SetFocus(dttm.ClientID);
                }
                else
                {
                    Nullable<DateTime> apptDttm = AppointmentDateTime(row);
                    string mrn = GetHiddenString(row, "hdn_apptMrn");
                    int id = ChangeAppointment(0, mrn, apptDttm, CommonCode.NullIfBlank((row.FindControl("txtSite") as TextBox).Text),
                                                                  CommonCode.NullIfBlank((row.FindControl("txtLocation") as TextBox).Text));
                    if (id > 0)
                    {
                        HDTICKTHIS = id.ToString();
                        gv_appointments.DataSource = DT_Appointments(mrn);
                        gv_appointments.DataBind();
                        HDTICKTHIS = null;
                    }
                }
            }
        }

        protected void btn_apptDelete_Command(object sender, CommandEventArgs e)
        {
            int iRow = 0;
            if (Int32.TryParse((sender as Button).CommandArgument, out iRow))
            {
                GridViewRow row = gv_appointments.Rows[iRow];
                int apptId = GetHiddenInt(row, "hdn_apptId");
                string mrn = GetHiddenString(row, "hdn_apptMrn");
                ChangeAppointment(apptId, mrn, null, "", "");
                gv_appointments.DataSource = DT_Appointments(mrn);
                gv_appointments.DataBind();
            }
        }

        protected void btn_apptUpdate_Command(object sender, CommandEventArgs e)
        {
            int iRow = 0;
            if (Int32.TryParse((sender as Button).CommandArgument, out iRow))
            {
                GridViewRow row = gv_appointments.Rows[iRow];
                TextBox dttm = row.FindControl("txtDttm") as TextBox;
                if (dttm.Text.Trim() == "")
                {
                    dttm.BackColor = System.Drawing.Color.Yellow;
                    Page.SetFocus(dttm.ClientID);
                }
                else
                {
                    Nullable<DateTime> apptDttm = AppointmentDateTime(row);
                    int apptId = GetHiddenInt(row, "hdn_apptId");
                    string mrn = GetHiddenString(row, "hdn_apptMrn");
                    int id = ChangeAppointment(apptId, mrn, apptDttm, CommonCode.NullIfBlank((row.FindControl("txtSite") as TextBox).Text),
                                                                      CommonCode.NullIfBlank((row.FindControl("txtLocation") as TextBox).Text));
                    if (id == apptId)
                    {
                        HDTICKTHIS = id.ToString();
                        gv_appointments.DataSource = DT_Appointments(mrn);
                        gv_appointments.DataBind();
                        HDTICKTHIS = null;
                    }
                }
            }
        }

        private Nullable<DateTime> AppointmentDateTime(GridViewRow row)
        {
            Nullable<DateTime> result = null;
            DateTime thisDate;
            DropDownList dl = row.FindControl("ddl_apptHours") as DropDownList;
            int hours = (dl.SelectedItem != null ? Convert.ToInt32(dl.SelectedItem.Text) : 0);
            dl = row.FindControl("ddl_apptMins") as DropDownList;
            int mins = (dl.SelectedItem != null ? Convert.ToInt32(dl.SelectedItem.Text) : 0);
            if (DateTime.TryParse((row.FindControl("txtDttm") as TextBox).Text, out thisDate))
            {
                result = thisDate.AddHours(hours).AddMinutes(mins);
            }
            return result;
        }
        #endregion

        private void SendAMS(string template, string destination, string[] appointments)
        {
            string apiKey = HDAPIKEY;

            var now = DateTime.UtcNow;
            HMAC signer = new HMACSHA256(Encoding.UTF8.GetBytes(HDSECRETKEY));
            string sigString = "AMSWebServiceSendMessages" + now.ToString("yyyy-MM-ddTHH:mm:ss.fffZ");
            byte[] toSign = Encoding.UTF8.GetBytes(sigString);
            string signature = Convert.ToBase64String(signer.ComputeHash(toSign));

            var client = new AMSWebService.AMSWebServiceClient();

            var msg = new AMSWebService.Message();
            msg.TemplateId = template;

            var address = new AMSWebService.Address();
            address.Value = destination;
            address.Type = AMSWebService.AddressType.user;

            List<AMSWebService.Address> addresses = new List<AMSWebService.Address>();
            addresses.Add(address);

            msg.Address = addresses.ToArray<AMSWebService.Address>();

            List<AMSWebService.Message> messages = new List<AMSWebService.Message>();
            foreach (string appointment in appointments)
            {
                msg.CustomId = Guid.NewGuid().ToString();
                var xml = XElement.Parse(appointment);
                var doc = new XmlDocument();
                var msgXml = doc.ReadNode(xml.CreateReader()) as XmlElement;
                msg.Content = msgXml;
                messages.Add(msg);
            }
            AMSWebService.MessageResult[] results;
            AMSWebService.Status status = client.SendMessages(apiKey, now, signature, messages.ToArray<AMSWebService.Message>(), false, out results);
            CommonCode.WriteLog(status.Code.ToString() + " - " + status.Description, "Test");
            if (status.Code == 0)
            {
                HDALERT = "Message successfully sent";
            }
            else
            {
                HDALERT = "Message failed with error: " + status.Code.ToString() + " - " + status.Description;
            }
            LogSendMessage(status, messages, results);
        }

        private string ReceiveAMS()
        {
            // Testing....
            //Thread.Sleep(2000);
            //return "2,3,4";
            //Testing .....

            string apiKey = HDAPIKEY;

            var now = DateTime.UtcNow;
            HMAC signer = new HMACSHA256(Encoding.UTF8.GetBytes(HDSECRETKEY));
            string sigString = "AMSWebServiceGetMessages" + now.ToString("yyyy-MM-ddTHH:mm:ss.fffZ");
            byte[] toSign = Encoding.UTF8.GetBytes(sigString);
            string signature = Convert.ToBase64String(signer.ComputeHash(toSign));

            var client = new AMSWebService.AMSWebServiceClient();

            //var msg = new AMSWebService.Message();
            //msg.TemplateId = template;

            //var address = new AMSWebService.Address();
            //address.Value = destination;
            //address.Type = AMSWebService.AddressType.user;

            //List<AMSWebService.Address> addresses = new List<AMSWebService.Address>();
            //addresses.Add(address);

            //msg.Address = addresses.ToArray<AMSWebService.Address>();

            //List<AMSWebService.Message> messages = new List<AMSWebService.Message>();
            //foreach (string appointment in appointments)
            //{
            //    var xml = XElement.Parse(appointment);
            //    var doc = new XmlDocument();
            //    var msgXml = doc.ReadNode(xml.CreateReader()) as XmlElement;
            //    msg.Content = msgXml;
            //    messages.Add(msg);
            //}

            //string[] sArray = { source };
            //List<AMSWebService.SearchFilter> filters = new List<AMSWebService.SearchFilter>();
            //var filter = new AMSWebService.SearchFilter();
            //filter.FilterBy = AMSWebService.FilterType.mobileAccount;
            //filter.ValueType = AMSWebService.FilterValueType.name;
            //filter.Value = sArray;
            //filters.Add(filter);
            AMSWebService.Message[] messages = null;
            AMSWebService.Status status = null;
            try
            {
                status = client.GetMessages(apiKey, now, signature, false, (AMSWebService.SearchFilter[])null, AMSWebService.LogicalOperator.and, false, out messages); //filters.ToArray<AMSWebService.SearchFilter>(), AMSWebService.LogicalOperator.and, false, out messages);
                if (status != null)
                    CommonCode.WriteLog(status.Code.ToString() + " - " + status.Description, "ReceiveAMS");
                else
                    CommonCode.WriteLog("ERROR: status=null", "ReceiveAMS");
            }
            catch (Exception ex)
            {
                CommonCode.WriteLog("ERROR: " + ex.Message, "ReceiveAMS");
            }
            return LogGetMessage(status, messages);

        }

        protected void gv_receivedMessages_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (HDEXPANDLOG == GetHiddenString(e.Row, "hdn_msgId"))
                {
                    e.Row.FindControl("lbl_msgContent").Visible = false;
                    e.Row.FindControl("pnl_msgContent").Visible = true;
                    (e.Row.FindControl("lit_msgContent") as Literal).Text = Server.HtmlEncode(FormatXml(FormatXml("<amsMessage id=\"" + (e.Row.FindControl("lbl_messageId") as Label).Text + "\">" + (e.Row.FindControl("hdn_msgContent") as HiddenField).Value + "</amsMessage>")));
                }
                else
                {
                    (e.Row.FindControl("lbl_msgContent") as Label).Text = Server.HtmlEncode(GetHiddenString(e.Row, "hdn_ShortContent"));
                }
                e.Row.Attributes.Add("onclick", ClientScript.GetPostBackEventReference(gv_receivedMessages, "Select$" + e.Row.RowIndex.ToString()));
            }
        }

        protected void gv_receivedMessages_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow row = gv_receivedMessages.SelectedRow;
            if (row != null)
            {
                HDEXPANDLOG = GetHiddenString(row, "hdn_msgId");
                gv_receivedMessages.DataSource = DT_GetMessages(HDNEWMESSAGES);
                gv_receivedMessages.DataBind();
            }
        }
        private string FormatXml(string input)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(input);
            using (StringWriter buffer = new StringWriter())
            {
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Indent = true;
                using (XmlWriter writer = XmlWriter.Create(buffer, settings))
                {
                    doc.WriteTo(writer);
                    writer.Flush();
                }
                buffer.Flush();
                return buffer.ToString();
            }
        }

        #region SQL ROUTINES
        private DataTable DT_AllAppointments(string orderBy)
        {
            string sortBy = (orderBy == "" ? "null" : "'" + orderBy.Replace("'", "''") + "'");
            string sql = "exec dbo.rp_getAppointments @fromDate=null,@sortBy=" + sortBy;
            return ExecuteSql(sql);
        }

        private DataTable DT_Patients()
        {
            string sql = "select ptMrn,dbo.rpFn_getPatientDisplayName(ptMrn) as [Patient] from tblPatient order by ptFamilyName,ptGivenName";
            DataTable dt = ExecuteSql(sql);
            if (dt != null)
            {
                DataRow newRow = dt.NewRow();
                newRow["ptMrn"] = "";
                newRow["Patient"] = ".. New";
                dt.Rows.Add(newRow);
            }
            return dt;
        }

        private DataTable DT_Appointments(string mrn)
        {
            string sql = "select apptId,apptMrn,apptDttm,apptSite,apptLocation from tblAppointment where apptMrn='" + mrn.Replace("'","''") + "' order by apptDttm";
            DataTable dt = ExecuteSql(sql);
            if (dt != null)
            {
                DataRow newRow = dt.NewRow();
                newRow["apptId"] = 0;
                newRow["apptMrn"] = mrn;
                //newRow["apptDttm"] = "";
                newRow["apptSite"] = "";
                newRow["apptLocation"] = "";
                dt.Rows.Add(newRow);
            }
            return dt;
        }

        private DataTable DT_QueryLog()
        {
            DataSet ODs = new DataSet();
            string Sqlcon = ConfigurationManager.AppSettings["HDConnection"].ToString();
            SqlConnection ocon = new SqlConnection(Sqlcon);
            SqlCommand ocmd = new SqlCommand();
            ocmd.Connection = ocon;
            ocmd.CommandType = CommandType.StoredProcedure;
            ocmd.CommandText = "rp_getQueryLog";
            ocon.Open();
            SqlDataAdapter oDA = new SqlDataAdapter();
            oDA.Fill(ODs, "tbl_Log");
            ocon.Close();
            return ODs.Tables[0];
        }

        private DataTable DT_GetMessages(string szIn)
        {
            string sql = "exec rp_getMessageLog @theseMessageIds='" + szIn + "'";
            return ExecuteSql(sql);
        }

        private void LogSendMessage(AMSWebService.Status status, List<AMSWebService.Message> messages, AMSWebService.MessageResult[] results)
        {
            string direction = "Send";
            try
            {
                string sql = "";
                for (int i = 0; i < messages.Count(); i++)
                {
                    sql = "insert into tblMessageLog (msgDirection,msgResult,msgResultDescription,msgMessageId,msgCustomId,msgTemplateId,msgContent,msgDttm) values (";
                    sql += CommonCode.SqlNullIfBlank(direction, "") + "," + status.Code.ToString() + CommonCode.SqlNullIfBlank(status.Description, ",");
                    if (status.Code == 0 && results != null && results.Count() > i)
                        sql += CommonCode.SqlNullIfBlank(results[i].MessageId, ",");
                    else
                        sql += ",NULL";
                    sql += CommonCode.SqlNullIfBlank(messages[i].CustomId, ",") + CommonCode.SqlNullIfBlank(messages[i].TemplateId, ",");
                    sql += CommonCode.SqlNullIfBlank(messages[i].Content.InnerXml, ",") + ",getdate())\r\n";
                    sql += "select convert(int,@@identity) as msgId";
                    DataTable dt = ExecuteSql(sql);
                    if (dt != null)
                    {
                        int msgId = (int)(dt.Rows[0]["msgId"]);
                        for (int n = 0; n < messages[i].Address.Count(); n++)
                        {
                            sql = "insert into tblMessageAddressLog (maId,maNo,maAddress,maAddressType) values (" + msgId.ToString() + "," + n.ToString();
                            sql += CommonCode.SqlNullIfBlank(messages[i].Address[n].Value, ",") + CommonCode.SqlNullIfBlank(messages[i].Address[n].Type.ToString(), ",") + ")";
                            ExecuteSql(sql);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonCode.WriteLog("ERROR: " + ex.Message, "LogMessage (Send)");
            }
        }

        private string LogGetMessage(AMSWebService.Status status, AMSWebService.Message[] messages)
        {
            string result = "";
            string direction = "Receive";
            try
            {
                string sql = "";
                if (messages != null)
                {
                    for (int i = 0; i < messages.Count(); i++)
                    {
                        sql = "insert into tblMessageLog (msgDirection,msgResult,msgResultDescription,msgMessageId,msgCustomId,msgTemplateId,msgContent,msgCreated,msgDttm) values (";
                        sql += CommonCode.SqlNullIfBlank(direction, "");
                        if (status != null)
                            sql += "," + status.Code.ToString() + CommonCode.SqlNullIfBlank(status.Description, ",");
                        else
                            sql += ",null,null";
                        sql += CommonCode.SqlNullIfBlank(messages[i].MessageId, ",") + CommonCode.SqlNullIfBlank(messages[i].CustomId, ",");
                        sql += CommonCode.SqlNullIfBlank(messages[i].TemplateId, ",") + CommonCode.SqlNullIfBlank(messages[i].Content.InnerXml, ",");
                        sql += ",'" + messages[i].Created.ToLocalTime().ToString("d-MMM-yyyy HH:mm:ss") + "',getdate())\r\n";
                        sql += "select convert(int,@@identity) as msgId";
                        DataTable dt = ExecuteSql(sql);
                        if (dt != null)
                        {
                            int msgId = (int)(dt.Rows[0]["msgId"]);
                            result += (result != "" ? "," : "") + msgId.ToString();
                            for (int n = 0; n < messages[i].Address.Count(); n++)
                            {
                                sql = "insert into tblMessageAddressLog (maId,maNo,maAddress,maAddressType) values (" + msgId.ToString() + "," + n.ToString();
                                sql += CommonCode.SqlNullIfBlank(messages[i].Address[n].Value, ",") + CommonCode.SqlNullIfBlank(messages[i].Address[n].Type.ToString(), ",") + ")";
                                ExecuteSql(sql);
                            }

                            try {
                                //Load XML   Cleanup and append XML structure to messages
                                XmlDocument xDoc = new XmlDocument();
                                string x = CommonCode.SqlNullIfBlank(messages[i].Content.InnerXml, ",");
                                x = x.TrimStart(',').Trim('\'');
                                x = "<root>" + x + "</root>"; 
                                x = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + x;
                                xDoc.LoadXml(x);
                                XmlNode pNode = xDoc.LastChild;

                                //Parse Feilds
                                XmlNode xmlMRN = pNode.SelectSingleNode("mRN");
                                String MRN = "";
                                if (xmlMRN != null) {    
                                MRN= xmlMRN.InnerText;
                                }

                                //Save Fields
                                if (MRN != "") {
                                    SaveFieldsRecursive(pNode, MRN, msgId, "NULL");
                                }
                            } catch (Exception exx) {
                                CommonCode.WriteLog("ERROR: " + exx.Message + "\r\nStack Trace: " + exx.StackTrace, "LogMessage (Receive)");
                            }
                        }
                    }
                }
                else
                {
                    sql = "insert into tblMessageLog (msgDirection,msgResult,msgResultDescription,msgDttm) values (";
                    if (status != null)
                        sql += CommonCode.SqlNullIfBlank(direction, "") + "," + status.Code.ToString() + CommonCode.SqlNullIfBlank(status.Description, ",") + ",getdate())";
                    else
                        sql += CommonCode.SqlNullIfBlank(direction, "") + ",null,null,getdate())";
                    ExecuteSql(sql);
                }
            }
            catch (Exception ex)
            {
                CommonCode.WriteLog("ERROR: " + ex.Message + "\r\nStack Trace: " + ex.StackTrace, "LogMessage (Receive)");
            }
            return result;
        }

        private void SaveFieldsRecursive(XmlNode pNode ,String MRN,int msgId,string parentMFId)
        {
            try {
                XmlNodeList cNodes = pNode.ChildNodes;
                DataTable dt = null;
                if (cNodes.Count > 0) {
                    string val = pNode.Value;
                    if (cNodes.Count == 1) {
                        if (!cNodes[0].HasChildNodes ) {
                                val = pNode.InnerText;
                        } else {
                            val = "";
                        }
                    }
                    else {
                        val = "";
                    }

                    if (pNode.Name == "photos") {
                        string dkkd = "";
                    }
                    string sql = "";
                    sql = "insert into tblMessageFieldValue (maId,ptMrn,field,value,parentField) values (" + msgId.ToString() + "," + MRN.ToString() + ",";
                    sql += "'" + pNode.Name.ToString() + "','" + val.ToString() + "'," + parentMFId + ")\r\n";
                    sql += "select convert(int,@@identity) as mfId";
                    dt = ExecuteSql(sql);
                    if (dt != null) {
                        int pMFId = (int)(dt.Rows[0]["mfId"]);
                        for (int n = 0; n < cNodes.Count; n++) {
                            SaveFieldsRecursive(cNodes[n], MRN, msgId, pMFId.ToString());
                        }
                    }
                }
            } catch (Exception ex) {
                CommonCode.WriteLog("ERROR: " + ex.Message + "\r\nStack Trace: " + ex.StackTrace, "LogMessage (Receive)");
            }
        }

        private DataTable ExecuteSql(string sql)
        {
            DataSet ODs = new DataSet();
            try
            {
                string Sqlcon = ConfigurationManager.AppSettings["HDConnection"].ToString();
                SqlConnection ocon = new SqlConnection(Sqlcon);
                ocon.Open();
                SqlDataAdapter oDA = new SqlDataAdapter(sql, ocon);
                oDA.Fill(ODs);
                ocon.Close();
            }
            catch (Exception ex)
            {
                CommonCode.WriteLog("sql=\"" + sql + "\"\r\n\r\nMessage: " + ex.Message + "\r\n\r\nStack Trace: " + ex.StackTrace, "ExecuteSql");
            }
            if (ODs.Tables.Count > 0)
                return ODs.Tables[0];
            else
                return null;
        }

        public bool ChangePatient(int type, string mrn, string family, string given, string szDob, string gender, string address1, string address2, string suburb, string postcode)
        {
            Nullable<DateTime> dob = null;
            DateTime actDob = DateTime.Today;
            if (DateTime.TryParse(szDob, out actDob))
                dob = actDob;
            DataSet ODs = new DataSet();
            string Sqlcon = ConfigurationManager.AppSettings["HDConnection"].ToString();
            SqlConnection ocon = new SqlConnection(Sqlcon);
            SqlCommand ocmd = new SqlCommand();
            ocmd.Connection = ocon;
            ocmd.CommandType = CommandType.StoredProcedure;
            ocmd.CommandText = "rp_createUpdatePatient";
            ocon.Open();
            ocmd.Parameters.Add("@addRequest", SqlDbType.Int).Value = type;
            ocmd.Parameters.Add("@mrn", SqlDbType.VarChar, 20).Value = CommonCode.NullIfBlank(mrn);
            ocmd.Parameters.Add("@given", SqlDbType.VarChar, 50).Value = CommonCode.NullIfBlank(given);
            ocmd.Parameters.Add("@family", SqlDbType.VarChar, 50).Value = CommonCode.NullIfBlank(family);
            ocmd.Parameters.Add("@dob", SqlDbType.Date).Value = dob;
            ocmd.Parameters.Add("@gender", SqlDbType.VarChar, 20).Value = CommonCode.NullIfBlank(gender);
            ocmd.Parameters.Add("@address1", SqlDbType.VarChar, 100).Value = CommonCode.NullIfBlank(address1);
            ocmd.Parameters.Add("@address2", SqlDbType.VarChar, 100).Value = CommonCode.NullIfBlank(address2);
            ocmd.Parameters.Add("@suburb", SqlDbType.VarChar, 50).Value = CommonCode.NullIfBlank(suburb);
            ocmd.Parameters.Add("@postcode", SqlDbType.VarChar, 10).Value = CommonCode.NullIfBlank(postcode);
            SqlParameter param = new SqlParameter("@result", SqlDbType.Int);
            param.Direction = ParameterDirection.ReturnValue;
            param.Value = 0;
            ocmd.Parameters.Add(param);
            ocmd.ExecuteNonQuery();
            int result = (int)ocmd.Parameters["@result"].Value;
            ocon.Close();
            return (result != 0);
        }

        public int ChangeAppointment(int apptId, string mrn, Nullable<DateTime> dttm, string site, string location)
        {
            DataSet ODs = new DataSet();
            string Sqlcon = ConfigurationManager.AppSettings["HDConnection"].ToString();
            SqlConnection ocon = new SqlConnection(Sqlcon);
            SqlCommand ocmd = new SqlCommand();
            ocmd.Connection = ocon;
            ocmd.CommandType = CommandType.StoredProcedure;
            ocmd.CommandText = "rp_createUpdateAppointment";
            ocon.Open();
            ocmd.Parameters.Add("@apptId", SqlDbType.Int).Value = apptId;
            ocmd.Parameters.Add("@mrn", SqlDbType.VarChar, 20).Value = CommonCode.NullIfBlank(mrn);
            ocmd.Parameters.Add("@dttm", SqlDbType.DateTime).Value = dttm;
            ocmd.Parameters.Add("@site", SqlDbType.VarChar, 20).Value = CommonCode.NullIfBlank(site);
            ocmd.Parameters.Add("@location", SqlDbType.VarChar, 100).Value = CommonCode.NullIfBlank(location);
            SqlParameter param = new SqlParameter("@result", SqlDbType.Int);
            param.Direction = ParameterDirection.ReturnValue;
            param.Value = 0;
            ocmd.Parameters.Add(param);
            ocmd.ExecuteNonQuery();
            int result = (int)ocmd.Parameters["@result"].Value;
            ocon.Close();
            return result;
        }
        #endregion

        #region COMMON GRIDVIEW FUNCTIONS

        private int GetHiddenInt(GridViewRow row, string fieldName)
        {
            int result = 0;
            HiddenField hf = row.FindControl(fieldName) as HiddenField;
            if (hf != null)
                Int32.TryParse(hf.Value, out result);
            return result;
        }

        private bool GetHiddenBit(GridViewRow row, string fieldName)
        {
            bool result = false;
            HiddenField hf = row.FindControl(fieldName) as HiddenField;
            if (hf != null)
                result = (hf.Value.ToLower() == "true");
            return result;
        }

        private string GetHiddenString(GridViewRow row, string fieldName)
        {
            HiddenField hf = row.FindControl(fieldName) as HiddenField;
            if (hf != null)
                return hf.Value;
            return "";
        }

        private string GetLabelString(GridViewRow row, string fieldName)
        {
            string result = "";
            Label lbl = row.FindControl(fieldName) as Label;
            if (lbl != null)
                result = lbl.Text;
            return result;
        }

        private Nullable<DateTime> GetTextBoxDate(GridViewRow row, string fieldName)
        {
            TextBox tb = row.FindControl(fieldName) as TextBox;
            if (tb == null) return null;
            DateTime result = DateTime.Today;
            if (DateTime.TryParse(tb.Text, out result))
                return result;
            else
                return null;
        }

        private string GetCellString(GridViewRow row, int cellNo)
        {
            string result = row.Cells[cellNo].Text;
            return result.Replace("&nbsp;", "");
        }
        #endregion
    }
}