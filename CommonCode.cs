﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Security.Principal;

namespace HealthDemo
{
    public class CommonCode
    {
        public static int logLevel
        {
            get
            {
                int result = 0;
                try
                {
                    Int32.TryParse(ConfigurationManager.AppSettings["LogLevel"], out result);
                }
                catch { };
                return result;
            }
        }

        public static void WriteLog(string logData, string logType)
        {
            //string Sqlcon = ConfigurationManager.AppSettings["ODSConnection"].ToString();
            //SqlConnection ocon = new SqlConnection(Sqlcon);
            //string sql = "insert into ZZTempLog (rpLogType,rpLogValue,rpLogDttm) values ('" + logType.Replace("'", "''") + "','" + logData.Replace("'", "''") + "',GETDATE())";
            //SqlCommand ocmd = new SqlCommand(sql, ocon);
            //ocon.Open();
            //ocmd.ExecuteNonQuery();
            //ocon.Close();

            string logPath = ConfigurationManager.AppSettings["LogFile"].ToString();
            if (logPath != "")
            {
                File.AppendAllText(logPath, "\r\n" + DateTime.Now.ToString("d-MMM-yyyy HH:mm:ss") + "  [" + logType + "]:\r\n" + logData);
            }
        }

        public static string SetDynamicButtonBackground(LinkButton btn, bool hBlue)
        {
            return (hBlue) ? btn.Text.Replace("\"Lg\"", "\"L\"").Replace("\"Cg\"", "\"C\"").Replace("\"Rg\"", "\"R\"") : btn.Text.Replace("\"L\"", "\"Lg\"").Replace("\"C\"", "\"Cg\"").Replace("\"R\"", "\"Rg\"");
        }

        public static string CookieName()
        {
            if (ConfigurationManager.AppSettings["Cookie"] != null)
                return ConfigurationManager.AppSettings["Cookie"].ToString();
            else
                return "EHF-MelbWater";
        }

        public static string BlankIfNull(object id)
        {
            if (id == null) return "";
            return id.ToString();
        }

        public static int ZeroIfNull(string sVal)
        {
            int result = 0;
            if (sVal != null)
                Int32.TryParse(sVal, out result);
            return result;
        }

        public static string NullIfBlank(object id)
        {
            if (id == null) return null;
            if (id.ToString().Length == 0) return null;
            return id.ToString();
        }

        public static string SqlNullIfBlank(string szVal, string delim)
        {
            if (szVal == null)
                return delim + "NULL";
            if (szVal.Trim().Length == 0)
                return delim + "NULL";
            else
                return delim + "'" + szVal.Trim().Replace("'", "''") + "'";
        }

        public static string SqlNullIfBlank(string szVal, string delim, int maxLth)
        {
            if (szVal == null)
                return delim + "NULL";
            if (szVal.Trim().Length == 0)
                return delim + "NULL";
            if (szVal.Trim().Length > maxLth)
                return delim + "'" + szVal.Trim().Substring(1, maxLth).Replace("'", "''") + "'";
            else
                return delim + "'" + szVal.Trim().Replace("'", "''") + "'";
        }

        public static bool FalseIfNull(object id)
        {
            if (id == null) return false;
            return (id.ToString() == "true");
        }

        public static string NullIfFalse(bool id)
        {
            return id ? "true" : null;
        }

        public static Nullable<int> NullIntIfBlank(string numb)
        {
            if (numb == "") return null;
            int n = 0;
            if (!Int32.TryParse(numb, out n)) return null;
            return n;
        }

        public static Nullable<int> NullIntIfBlankOrZero(string numb)
        {
            if (numb == "") return null;
            int n = 0;
            if (!Int32.TryParse(numb, out n)) return null;
            if (n == 0)
                return null;
            else
                return n;
        }

        public static string HtmlBlankIfNull(string szVal)
        {
            if (szVal == null)
                return "&nbsp;";
            else if (szVal == "")
                return "&nbsp;";
            else
                return szVal;
        }

        public static Nullable<DateTime> DateFromString(string sDate)
        {
            DateTime myDate;
            if (DateTime.TryParse(sDate, out myDate))
                return myDate;
            else
                return null;
        }

        public static string GetStringElement(string[] sArray, int index)
        {
            if (sArray.Length > index)
                return sArray[index].Trim();
            else
                return "";
        }

        public static System.Drawing.Color GetBackgroundColour(string hexColour)
        {
            if (hexColour.Length < 6)
                return System.Drawing.Color.Transparent;
            else
                return System.Drawing.Color.FromArgb(Convert.ToInt32(hexColour.Substring(0, 2), 16), Convert.ToInt32(hexColour.Substring(2, 2), 16), Convert.ToInt32(hexColour.Substring(4, 2), 16));
        }

        #region STATE ROUTINES
        public static string StatePush(string current, string newLevel)
        {
            string result = current;
            if (current != "") current += "\n";
            return current + newLevel;
        }

        public static string StatePop(string current)
        {
            int lastIndex = current.LastIndexOf('\n');
            if (lastIndex > 0)
                return current.Substring(0, lastIndex);
            else
                return "";
        }

        public static string StateTop(string current)
        {
            string[] levels = current.Split('\n');
            return levels[levels.Length - 1];
        }

        public static string StateUpdate(string current, string topLevelUpdate)
        {
            string[] levels = current.Split('\n');
            levels[levels.Length - 1] = topLevelUpdate;
            return string.Join("\n", levels);
        }

        public static string[] StateTopSplit(string current)
        {
            string[] levels = current.Split('\n');
            string temp = levels[levels.Length - 1].Replace("&quot;", "\"").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&amp;", "&");
            return temp.Split('\t');
        }

        public static string StateGetProjectNumber(string current)
        {
            string[] levels = current.Split('\n');
            return GetStringElement(GetStringElement(levels, 1).Split('\t'), 1);
        }

        public static int StateGetProgramId(string current)
        {
            string[] levels = current.Split('\n');
            int programId = 0;
            Int32.TryParse(GetStringElement(levels[0].Split('\t'), 1), out programId);
            return programId;
        }

        public static string StateSetLevel(string current, int level)
        {
            if (level == 0)
                return "";
            else
            {
                string result = current;
                int index = result.IndexOf('\n');
                while (index >= 0 && (--level) > 0)
                    index = result.IndexOf('\n', index + 1);
                if (index >= 0)
                    result = result.Substring(0, index);
                return result;
            }
        }
        #endregion

        #region SESSION VARIABLES
        /// <summary>
        /// Attempts to convert a string to an integer, returns -1 if it cant.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static int strInt(string s)
        {
            int v;
            var suc = int.TryParse(s, out v);
            if (!suc)
            {
                v = -1;
            }
            return v;
        }
        public static int? strNInt(string s)
        {
            int? v = null;
            int tempInt = -1;
            var suc = int.TryParse(s, out tempInt);
            if (suc && tempInt != -1)
            {
                v = tempInt;
            }
            return v;
        }
        #endregion
    }
}