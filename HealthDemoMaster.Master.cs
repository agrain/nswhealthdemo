﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace HealthDemo
{
    public partial class HealthDemoMaster : System.Web.UI.MasterPage
    {

        #region PAGE PROPERTIES
        private int SessINT(string n)
        {
            if (Session[n] != null)
            {
                return CommonCode.strInt(Session[n].ToString());
            }
            else
            {
                return -1;
            }
        }
        private Nullable<int> SessINTnull(string n)
        {
            if (Session[n] != null)
            {
                string bID = Session[n].ToString();
                return CommonCode.strInt(bID);
            }
            else
                return null;
        }
        public int ACCID { get { return SessINT("accID"); } set { Session["accID"] = value; } }
        public string HDPROFILE
        {
            get { return CommonCode.BlankIfNull(Session["HDProfile"]); }
            set { Session["HDProfile"] = CommonCode.NullIfBlank(value); }
        }
        public string HDSTATE
        {
            get { return CommonCode.BlankIfNull(Session["HDState"]); }
            set { Session["HDState"] = CommonCode.NullIfBlank(value); }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack == false)
            {
                try
                {
                    setValidStylesheets();
                    Page.Title = "NSW Health - AMS Demo";
                    //if (SPRPROFILE == "")
                    //    SPRPROFILE = CommonCode.Authenticate();
                    lbl_PageBanner.Text = "NSW Health Demo";
                    appVersion.InnerHtml = ConfigurationManager.AppSettings["Version"].ToString();
                    //footerLogo.Src = "images/MelbourneWater.png";
                    //footerLogo.Alt = "Melbourne Water";
                    footerDA.Visible = false;
                    poweredBy.Visible = true;
                    if (HDPROFILE.Split('\t')[0] != "")
                    {
                        lbl_User.Text = HDPROFILE.Split('\t')[0].ToString();
                        requests_pending_status.Visible = false;
                    }
                    lnk_Admin.Visible = false;
                    requests_pending_status.Visible = false;
                    lnkbtn_Bookingform.Visible = false;
                    lnk_logout.Visible = false;
                    string breadcrumb = "Home";
                    if (HDSTATE != "")
                    {
                        breadcrumb = "<a href=\"Default.aspx?level=0\">Home</a>";
                        string[] levels = HDSTATE.Split('\n');
                        for (int i = 0; i < levels.Length; i++)
                        {
                            if (i < (levels.Length - 1))
                                breadcrumb += " > <a href=\"Default.aspx?level=" + (i + 1).ToString() + "\">" + CommonCode.GetStringElement(levels[i].Split('\t'), 2) + "</a>";
                            else
                                breadcrumb += " > " + CommonCode.GetStringElement(levels[i].Split('\t'), 2);
                        }
                    }
                    headTitle.InnerHtml = breadcrumb;
                    //string[] state = CommonCode.StateTopSplit(MWSTATE);
                    //switch (state[0])
                    //{
                    //    case "":
                    //        headTitle.InnerHtml = "Programs";
                    //        break;

                    //    case "Program":
                    //        if (state[1] == "0")
                    //            headTitle.InnerHtml = "New Program";
                    //        else
                    //            headTitle.InnerHtml = "\"" + CommonCode.GetStringElement(MWSTATE.Split('\t'), 2) + "\" detail";
                    //        break;

                    //}
                    headMonth.InnerHtml = "";
                }
                catch (Exception EMaster)
                {
                    //Orfid.Usp_WebErrors("Homepage.Master", "PageLoad", DateTime.Now.AddDays(0), EMaster.Message.ToString() + "\r\n\nStack Trace: " + EMaster.StackTrace);
                }
            }
        }

        protected void lnk_logout_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            Response.Redirect("Default.aspx", false);
            return;
        }
        protected void lnk_profile_Click(object sender, EventArgs e)
        {
            //ClassRFID.StopReader();
            //Session["ReceiveType"] = "0";
            //Session.Remove("BatchID");
            //Session["NewTags"] = "0";
            //Session["NewManualTags"] = "0";
            //Response.Redirect("ProfileSettings.aspx", false);
        }
        protected void lnk_Admin_Click(object sender, EventArgs e)
        {
            //ClassRFID.StopReader();
            //Session["ReceiveType"] = "0";
            //Session.Remove("BatchID");
            //Session["NewTags"] = "0";
            //Session["NewManualTags"] = "0";
            //Response.Redirect("Users.aspx?qry=Admin");
        }
        protected void lnk_Home_Click(object sender, EventArgs e)
        {
            //MWHOMEVIEW = null;
            Session.Abandon();
            Response.Redirect("Default.aspx", false);
            return;
        }
        protected void lnkbtn_Bookingform_Click(object sender, EventArgs e)
        {
            //ClassRFID.StopReader();
            //InitializeSessionVariables();
            //Response.Redirect("BookingForm.aspx");
        }

        protected void setValidStylesheets()
        {
            string url = Request.Url.ToString();
            if (url.IndexOf("/Default.aspx", StringComparison.CurrentCultureIgnoreCase) >= 0)
            {
                style_default.Visible = true;
            }
            //if (url.IndexOf("/Procedure.aspx", StringComparison.CurrentCultureIgnoreCase) >= 0)
            //{
            //    style_procedure.Visible = style_jquery_ui.Visible = true;
            //    style_date_poly.Visible = true;  // Actually need datetime_poly for slade and date_poly for RFID
            //    style_datetime_poly.Visible = true;
            //}
            //style_user.Visible = (url.IndexOf("/Users.aspx", StringComparison.CurrentCultureIgnoreCase) >= 0);
            //if (url.IndexOf("/BookingForm.aspx", StringComparison.CurrentCultureIgnoreCase) >= 0)
            //{
            //    style_booking.Visible = style_jquery_ui.Visible = true;
            //    style_date_poly.Visible = true;  // Actually need datetime_poly for slade and date_poly for RFID
            //    style_datetime_poly.Visible = true;
            //}
            //style_admin.Visible = (url.IndexOf("/Administration.aspx", StringComparison.CurrentCultureIgnoreCase) >= 0);
            //style_profile.Visible = (url.IndexOf("/ProfileSettings.aspx", StringComparison.CurrentCultureIgnoreCase) >= 0);
        }

        private void InitializeSessionVariables()
        {
            //if (CommonCode.IdentifyHosting(Request.Url.ToString()) == HostType.eHealthFlowType.eHealthflow_SLADE)
            //{
            //    if (CommonCode.UsingHomePage() && (SUPID == null) && (SLADEORDERTYPE != "Oncology"))
            //    {
            //        Session["SladePatient"] = (Session["SladeOrderType"] == "Oncology") ? null : Orfid.rpFn_sladeGetPatientId(HOSPID, "000000").ToString() + "~ANALGESICS~~000000~";
            //    }
            //    else
            //    {
            //        Session["SladePatient"] = null;             // SLADEPATIENT
            //    }
            //    Session["SladeCurrentMedications"] = null;      // SLADEMEDS
            //    Session["SladeOrderWhen"] = null;               // SLADEORDERDATETIME
            //    Session["SladeOrderItemDetail"] = null;         // SLADEORDERITEMDETAIL
            //    Session["SladeOrderItemScript"] = null;         // SLADEORDERITEMSCRIPT
            //    Session["SladeOrderSaveCurrent"] = null;        // SLADEORDERSAVECURRENT
            //}
            //else
            //{
            //    Session["ReceiveType"] = "0";
            //    Session.Remove("BatchID");
            //    Session["NewTags"] = "0";
            //    Session["NewManualTags"] = "0";
            //}
        }
    }

}