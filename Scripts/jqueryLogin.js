$(document).ready(function () {
    if (!Modernizr.input.placeholder) {
        var placeholderText = $('#txt_Username').attr('placeholder');
        if ($('txt_Username').val() == '') {
            $('#txt_Username').attr('value', placeholderText);
            $('#txt_Username').addClass('placeholder');
        }
        $('#txt_Username').focus(function () {
            if (($('#txt_Username').val() == placeholderText)) {
                $('#txt_Username').attr('value', '');
                $('#txt_Username').removeClass('placeholder');
            }
        });
        $('#txt_Username').blur(function () {
            if (($('#txt_Username').val() == placeholderText) || (($('#txt_Username').val() == ''))) {
                $('#txt_Username').addClass('placeholder');
                $('#txt_Username').attr('value', placeholderText);
            }
        });
    }
    if (($('#txt_Username').val() == placeholderText) || (($('#txt_Username').val() == ''))) {
        $('#txt_Username').focus();
    } else {
        $('#Txt_Password').focus();
    }
});
