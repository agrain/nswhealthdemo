$(document).ready(function () {
    $(window).resize(function () {
        var w = $(document).width();
        if (w > 720) {
            $('body').css('width', '700px');
        } else {
            if (w < 460) {
                $('body').css('width', '450px');
            } else {
                $('body').css('width', '96%');
            }
        }
    });
    if (!Modernizr.input.placeholder) {
        var placeholderText = $('#txt_Username').attr('placeholder');
        if ($('txt_Username').val() == '') {
            $('#txt_Username').attr('value', placeholderText);
            $('#txt_Username').addClass('placeholder');
        }
        $('#txt_Username').focus(function () {
            if (($('#txt_Username').val() == placeholderText)) {
                $('#txt_Username').attr('value', '');
                $('#txt_Username').removeClass('placeholder');
            }
        });
        $('#txt_Username').blur(function () {
            if (($('#txt_Username').val() == placeholderText) || (($('#txt_Username').val() == ''))) {
                $('#txt_Username').addClass('placeholder');
                $('#txt_Username').attr('value', placeholderText);
            }
        });
    }
    if (($('#txt_Username').val() == placeholderText) || (($('#txt_Username').val() == ''))) {
        $('#txt_Username').focus();
    } else {
        $('#Txt_Password').focus();
    }
    $(window).resize();
});
