﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HealthDemoMaster.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="HealthDemo._Default" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" />

    <asp:Panel runat="server" ID="panel_AdminButtons">
        <div class="buttons-right">
            <asp:LinkButton ID="lnkbtn_PatientAdmin" runat="server" onclick="lnkbtn_PatientAdmin_Click" CssClass="button-dynamic FloatRight"><div id="L"></div><div id="C">&nbsp;&nbsp;Patient/Appointment Administration&nbsp;&nbsp;</div><div id="R"></div></asp:linkbutton>
            <asp:LinkButton ID="lnkbtn_ReceiveMessages" runat="server" onclick="lnkbtn_ReceiveMessages_Click" CssClass="button-dynamic FloatRight"><div id="L"></div><div id="C">&nbsp;&nbsp;Receive Messages&nbsp;&nbsp;</div><div id="R"></div></asp:linkbutton>
            <asp:LinkButton ID="lnkbtn_AllLogs" runat="server" onclick="lnkbtn_AllLogs_Click" CssClass="button-dynamic FloatRight"><div id="L"></div><div id="C">&nbsp;&nbsp;View All Logs&nbsp;&nbsp;</div><div id="R"></div></asp:linkbutton>
        </div>
    </asp:Panel>

    <asp:UpdatePanel runat="server" ID="panel_AllAppointments">
        <ContentTemplate>
            <div id="div_allAppointments" class="boxedLeftDiv">
                <table>
                    <tr runat="server" id="row_apptGridView">
                        <td>
                            <asp:GridView runat="server" ID="gv_allAppointments" AutoGenerateColumns="false" AllowSorting="true" OnSorting="gv_allAppointments_OnSorting">
                                <Columns>
                                    <asp:TemplateField HeaderText="Select">
                                        <ItemTemplate>
                                            <asp:CheckBox runat="server" ID="chkSelect" OnCheckedChanged="chkSelect_CheckedChanged" AutoPostBack="true" />
                                            <asp:HiddenField runat="server" ID="hdn_apptId" Value='<%#Eval("apptId") %>' />
                                            <asp:HiddenField runat="server" ID="hdn_apptMrn" Value='<%#Eval("apptMrn") %>' />
                                            <asp:HiddenField runat="server" ID="hdn_address" Value='<%#Eval("Address") %>' />
                                            <asp:HiddenField runat="server" ID="hdn_ptFamilyName" Value='<%#Eval("ptFamilyName") %>' />
                                            <asp:HiddenField runat="server" ID="hdn_ptGivenName" Value='<%#Eval("ptGivenName") %>' />
                                            <asp:HiddenField runat="server" ID="hdn_ptGender" Value='<%#Eval("ptGender") %>' />
                                            <asp:HiddenField runat="server" ID="hdn_ptDOB" Value='<%#Eval("ptDOB", "{0:d/MMM/yyyy}") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Appointment" DataField="apptDttm" DataFormatString="{0:d/MMM/yyyy HH:mm}" SortExpression="Sort_Dttm" />
                                    <asp:BoundField HeaderText="Patient" DataField="Patient" SortExpression="Sort_Patient" />
                                    <asp:BoundField HeaderText="Site" DataField="apptSite" SortExpression="Sort_Site" />
                                    <asp:BoundField HeaderText="Location" DataField="apptLocation" SortExpression="Sort_Location" />
                                </Columns>
                                <HeaderStyle BackColor="#025A8D" Font-Bold="True" ForeColor="#FFFFCC" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr runat="server" id="row_apptAction" visible="false">
                        <td>
                            <asp:LinkButton ID="lnkBtn_Send" runat="server" onclick="lnkBtn_Send_Click" CssClass="button-dynamic FloatRight"><div id="L"></div><div id="C">&nbsp;&nbsp;Send Appointment(s)&nbsp;&nbsp;</div><div id="R"></div></asp:linkbutton>
                        </td>
                    </tr>
                    <tr runat="server" id="row_apptNone">
                        <td><h1>There are no scheduled appointments</h1></td>
                    </tr>
                </table>
            </div>

            <asp:panel runat="server" ID="panel_AMSSend" Visible="false" CssClass="boxedLeftDiv">
                <table>
                    <tr>
                        <th colspan="2">Send Appointments to Tablet</th>
                    </tr>
                    <tr>
                        <td>Template:</td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddl_TemplateId" />
                        </td>
                    </tr>
                    <tr>
                        <td>Recipient:</td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddl_Destination" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:LinkButton ID="lnkBtn_transmit" runat="server" onclick="lnkBtn_transmit_Click" CssClass="button-dynamic FloatRight"><div id="L"></div><div id="C">&nbsp;&nbsp;Transmit Now&nbsp;&nbsp;</div><div id="R"></div></asp:linkbutton>
                        </td>
                    </tr>
                </table>
            </asp:panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel runat="server" ID="panel_receive">
        <ContentTemplate>
            <asp:Panel runat="server" ID="panel_AMSReceive" CssClass="boxedLeftDiv">
                <div id="div_receivedMessages">
                    <asp:GridView runat="server" ID="gv_receivedMessages" AutoGenerateColumns="false" GridLines="Both"
                                            OnRowDataBound="gv_receivedMessages_RowDataBound" OnSelectedIndexChanged="gv_receivedMessages_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField HeaderText="Message ID">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lbl_messageId" Text='<%#Eval("msgMessageId") %>' />
                                    <asp:HiddenField runat="server" ID="hdn_msgId" Value='<%#Eval("msgId") %>' />
                                    <asp:HiddenField runat="server" ID="hdn_msgCustomId" Value='<%#Eval("msgCustomId") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Template" DataField="Template" />
                            <asp:BoundField HeaderText="From" DataField="From" />
                            <asp:BoundField HeaderText="Created" DataField="msgCreated" DataFormatString="{0:d-MMM-yyyy HH:mm:ss}" />
                            <asp:TemplateField HeaderText="Content">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lbl_msgContent" />                                    
                                    <asp:Panel runat="server" ID="pnl_msgContent">
                                        <pre><asp:Literal runat="server" ID="lit_msgContent" Mode="PassThrough" /></pre>
                                    </asp:Panel>
                                    <asp:HiddenField runat="server" ID="hdn_ShortContent" Value='<%#Eval("ShortContent") %>' />
                                    <asp:HiddenField runat="server" ID="hdn_msgContent" Value='<%#Eval("msgContent") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle BackColor="White" ForeColor="Black" />
                        <AlternatingRowStyle BackColor="LightBlue" ForeColor="Black" />
                    </asp:GridView>
                </div>
                <asp:Label runat="server" ID="lbl_receiveNoMessages" Text="No messages were available on the gateway." Visible="false" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel runat="server" ID="panel_allLogs">
        <ContentTemplate>
            <asp:Panel runat="server" ID="panel_queryLogs" CssClass="boxedLeftDiv">
                <div id="div_queryLogs">
                    <asp:GridView runat="server" ID="gv_queryLogs" AutoGenerateColumns="false" Caption="Query Log" GridLines="Both"
                                            OnRowDataBound="gv_queryLogs_RowDataBound" OnSelectedIndexChanged="gv_queryLogs_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField HeaderText="Date/Time">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lbl_logDttm" Text='<%#Eval("logDttm", "{0:d/MMM/yyyy HH:mm}") %>' />
                                    <asp:HiddenField runat="server" ID="hdn_logId" Value='<%#Eval("logId") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField  HeaderText="Request">
                                <ItemTemplate>
                                    <asp:Label runat="server" id="lbl_logIn" />
                                    <asp:Panel runat="server" ID="pnl_logIn">
                                        <pre><asp:Literal runat="server" ID="lit_viewLogIn" Mode="PassThrough" /></pre>
                                    </asp:Panel>
                                    <asp:HiddenField runat="server" ID="hdn_shortIn" Value='<%#Eval("shortIn") %>' />
                                    <asp:HiddenField runat="server" ID="hdn_logIn" Value='<%#Eval("logIn") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField  HeaderText="Response">
                                <ItemTemplate>
                                    <asp:Label runat="server" id="lbl_logOut" />
                                    <asp:Panel runat="server" ID="pnl_logOut">
                                        <pre><asp:Literal runat="server" ID="lit_viewLogOut" Mode="PassThrough" /></pre>
                                    </asp:Panel>
                                    <asp:HiddenField runat="server" ID="hdn_shortOut" Value='<%#Eval("shortOut") %>' />
                                    <asp:HiddenField runat="server" ID="hdn_logOut" Value='<%#Eval("logOut") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle BackColor="White" ForeColor="Black" />
                        <AlternatingRowStyle BackColor="LightBlue" ForeColor="Black" />
                    </asp:GridView>
                </div>
            </asp:Panel>

            <asp:Panel runat="server" ID="panel_messageLogs" CssClass="boxedLeftDiv">
                <div id="div_messageLogs">
                    <asp:GridView runat="server" ID="gv_messageLogs" AutoGenerateColumns="false" Caption="Message Log" GridLines="Both"
                                            OnRowDataBound="gv_messageLogs_RowDataBound" OnSelectedIndexChanged="gv_messageLogs_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField HeaderText="Message ID">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lbl_messageId" Text='<%#Eval("msgMessageId") %>' />
                                    <asp:HiddenField runat="server" ID="hdn_msgId" Value='<%#Eval("msgId") %>' />
                                    <asp:HiddenField runat="server" ID="hdn_msgCustomId" Value='<%#Eval("msgCustomId") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Direction" DataField="msgDirection" />
                            <asp:BoundField HeaderText="Template" DataField="Template" />
                            <asp:BoundField HeaderText="From" DataField="From" />
                            <asp:BoundField HeaderText="Created" DataField="msgCreated" DataFormatString="{0:d-MMM-yyyy HH:mm:ss}" />
                            <asp:TemplateField HeaderText="Content">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lbl_msgContent" />                                    
                                    <asp:Panel runat="server" ID="pnl_msgContent">
                                        <pre><asp:Literal runat="server" ID="lit_msgContent" Mode="PassThrough" /></pre>
                                    </asp:Panel>
                                    <asp:HiddenField runat="server" ID="hdn_ShortContent" Value='<%#Eval("ShortContent") %>' />
                                    <asp:HiddenField runat="server" ID="hdn_msgContent" Value='<%#Eval("msgContent") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle BackColor="White" ForeColor="Black" />
                        <AlternatingRowStyle BackColor="LightBlue" ForeColor="Black" />
                    </asp:GridView>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel runat="server" ID="panel_patients">
        <ContentTemplate>
        <div id="div_patients" class="boxedLeftDiv">
            <h1>Patient</h1>
            <table>
                <tr runat="server" id="row_SelectPatient">
                    <td>Patient:</td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddl_patient" AutoPostBack="true" OnSelectedIndexChanged="ddl_patient_SelectedIndexChanged" />
                    </td>
                </tr>
                <tr runat="server" id="row_PatientMrn" Visible="false">
                    <td>MRN:</td>
                    <td>
                        <asp:TextBox runat="server" ID="txt_ptMrn" Columns="15" MaxLength="20" />
                    </td>
                </tr>
                <tr runat="server" id="row_PatientFamily" Visible="false">
                    <td>Family Name:</td>
                    <td>
                        <asp:TextBox runat="server" ID="txt_ptFamilyName" Columns="30" MaxLength="50" />
                    </td>
                </tr>
                <tr runat="server" id="row_PatientGiven" Visible="false">
                    <td>Given Name:</td>
                    <td>
                        <asp:TextBox runat="server" ID="txt_ptGivenName" Columns="30" MaxLength="50" />
                    </td>
                </tr>
                 <tr runat="server" id="row_PatientDOB" Visible="false">
                    <td>Date Of Birth:</td>
                    <td>
                        <asp:TextBox runat="server" ID="txt_ptDOB" Columns="15" />&nbsp;<asp:ImageButton ID="img_ptDob" runat="server" ImageUrl="~/Images/calendar-icon.jpg" ImageAlign="Top" width="15px" />
                        <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txt_ptDOB" Format="d/MMM/yyyy" PopupButtonID="txt_ptDOB"/>
                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txt_ptDOB" Format="d/MMM/yyyy" PopupButtonID="img_ptDob"/>
                     </td>
                </tr>
                 <tr runat="server" id="row_PatientGender" Visible="false">
                    <td>Gender:</td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddl_ptGender">
                            <asp:ListItem Selected="True">Male</asp:ListItem>
                            <asp:ListItem>Female</asp:ListItem>
                        </asp:DropDownList>
                     </td>
                </tr>
                <tr runat="server" id="row_PatientAddressLine1" Visible="false">
                    <td>Address1:</td>
                    <td>
                        <asp:TextBox runat="server" ID="txt_ptAddressLine1" Columns="40" MaxLength="100" />
                    </td>
                </tr>
                <tr runat="server" id="row_PatientAddressLine2" Visible="false">
                    <td>Address2:</td>
                    <td>
                        <asp:TextBox runat="server" ID="txt_ptAddressLine2" Columns="40" MaxLength="100" />
                    </td>
                </tr>
                <tr runat="server" id="row_PatientSuburb" Visible="false">
                    <td>Suburb/City:</td>
                    <td>
                        <asp:TextBox runat="server" ID="txt_ptSuburb" Columns="40" MaxLength="50" />
                    </td>
                </tr>
                <tr runat="server" id="row_PatientPostCode" Visible="false">
                    <td>PostCode:</td>
                    <td>
                        <asp:TextBox runat="server" ID="txt_ptPostCode" Columns="10" MaxLength="10" />
                    </td>
                </tr>
               <tr>
                    <td colspan="2">
                        <asp:LinkButton ID="lnkBtn_EditPatient" runat="server" OnClick="lnkBtn_EditPatient_Click" CssClass="button-dynamic FloatRight" Style="margin: 2px 5px;"><div id="L"></div><div id="C">&nbsp;&nbsp;Edit&nbsp;&nbsp;</div><div id="R"></div></asp:LinkButton>
                        <asp:LinkButton ID="lnkBtn_CancelPatient" runat="server" OnClick="lnkBtn_CancelPatient_Click" CssClass="button-dynamic FloatRight" Style="margin: 2px 5px;" Visible="false"><div id="L"></div><div id="C">&nbsp;&nbsp;Cancel&nbsp;&nbsp;</div><div id="R"></div></asp:LinkButton>
                    </td>
                </tr>
            </table>
        </div>

            
            <asp:Panel runat="server" ID="panel_Appointments" DefaultButton="lnkbtn_dummyAppointments">
                <div id="div_AdminDeliverySite" class="boxedLeftDiv">
                    <asp:GridView ID="gv_appointments" runat="server" AutoGenerateColumns="false" Caption="Appointments" OnRowDataBound="gv_appointments_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="Appointment Date/Time" ItemStyle-CssClass="NoPadding">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                        <td class="NoHorizPadding">
                                            <asp:TextBox runat="server" ID="txtDttm" Columns="15" Text='<%#Eval("apptDttm", "{0:d/MMM/yyyy}") %>' CssClass="NoMargin" />
                                            <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtDttm" Format="d/MMM/yyyy" PopupButtonID="txtDttm"/>
                                            <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtDttm" Format="d/MMM/yyyy" PopupButtonID="imgDttm"/>
                                        </td>
                                        <td class="NoHorizPadding">
                                            <asp:DropDownList runat="server" ID="ddl_apptHours" CssClass="NoMargin">
                                                <asp:ListItem>00</asp:ListItem>
                                                <asp:ListItem>01</asp:ListItem>
                                                <asp:ListItem>02</asp:ListItem>
                                                <asp:ListItem>03</asp:ListItem>
                                                <asp:ListItem>04</asp:ListItem>
                                                <asp:ListItem>05</asp:ListItem>
                                                <asp:ListItem>06</asp:ListItem>
                                                <asp:ListItem>07</asp:ListItem>
                                                <asp:ListItem>08</asp:ListItem>
                                                <asp:ListItem>09</asp:ListItem>
                                                <asp:ListItem>10</asp:ListItem>
                                                <asp:ListItem>11</asp:ListItem>
                                                <asp:ListItem>12</asp:ListItem>
                                                <asp:ListItem>13</asp:ListItem>
                                                <asp:ListItem>14</asp:ListItem>
                                                <asp:ListItem>15</asp:ListItem>
                                                <asp:ListItem>16</asp:ListItem>
                                                <asp:ListItem>17</asp:ListItem>
                                                <asp:ListItem>18</asp:ListItem>
                                                <asp:ListItem>19</asp:ListItem>
                                                <asp:ListItem>20</asp:ListItem>
                                                <asp:ListItem>21</asp:ListItem>
                                                <asp:ListItem>22</asp:ListItem>
                                                <asp:ListItem>23</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td class="NoHorizPadding">
                                            <asp:DropDownList runat="server" ID="ddl_apptMins" CssClass="NoMargin">
                                                <asp:ListItem>00</asp:ListItem>
                                                <asp:ListItem>05</asp:ListItem>
                                                <asp:ListItem>10</asp:ListItem>
                                                <asp:ListItem>15</asp:ListItem>
                                                <asp:ListItem>20</asp:ListItem>
                                                <asp:ListItem>25</asp:ListItem>
                                                <asp:ListItem>30</asp:ListItem>
                                                <asp:ListItem>35</asp:ListItem>
                                                <asp:ListItem>40</asp:ListItem>
                                                <asp:ListItem>45</asp:ListItem>
                                                <asp:ListItem>50</asp:ListItem>
                                                <asp:ListItem>55</asp:ListItem>
                                           </asp:DropDownList>
                                        </td>
                                        <td class="NoHorizPadding">
                                            <asp:ImageButton ID="imgDttm" runat="server" ImageUrl="~/Images/calendar-icon.jpg" ImageAlign="Top" width="15px" />
                                        </td>
                                        </tr>
                                    </table>
                                    <asp:HiddenField runat="server" ID="hdn_apptId" Value='<%#Eval("apptId") %>' />
                                    <asp:HiddenField runat="server" ID="hdn_apptMrn" Value='<%#Eval("apptMrn") %>' />
                                    <asp:HiddenField runat="server" ID="hdn_apptDttm" Value='<%#Eval("apptDttm", "{0:d/MMM/yyyy HH:mm}") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Site">
                                <ItemTemplate>
                                    <asp:TextBox runat="server" ID="txtSite" Columns="20" MaxLength="50" Text='<%#Eval("apptSite") %>'  />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Location">
                                <ItemTemplate>
                                    <asp:TextBox runat="server" ID="txtLocation" Columns="20" MaxLength="50" Text='<%#Eval("apptLocation") %>'  />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Actions" ItemStyle-CssClass="Text-alignCenter">
                                <ItemTemplate>
                                    <asp:Button ID="btn_apptUpdate" runat="server" ToolTip="Update Appointment" CssClass="icon-16-update buttons-small16 PadLeft20" Text="" OnCommand="btn_apptUpdate_Command" CommandArgument="<%# ((GridViewRow)Container).RowIndex %>" />
                                    <asp:Button ID="btn_apptAdd" runat="server" ToolTip="Add Appointment" CssClass="icon-16-add buttons-small16 PadLeft20" Text="" OnCommand="btn_apptAdd_Command" CommandArgument="<%# ((GridViewRow)Container).RowIndex %>" />
                                    <asp:Button ID="btn_apptDelete" runat="server" ToolTip="Delete Appointment" CssClass="icon-16-trash buttons-small16 PadLeft20" Text="" OnCommand="btn_apptDelete_Command" OnClientClick="return confirm('Are you certain you want to delete this Appointment');" CommandArgument="<%# ((GridViewRow)Container).RowIndex %>" />
                                    <asp:Button ID="btn_apptTick" runat="server" ToolTip="Appointment successfully updated" CssClass="icon-16-tick buttons-small16 PadLeft20" Text="" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                <asp:LinkButton runat="server" ID="lnkbtn_dummyAppointments" Visible="true" OnClientClick="javascript: return false;" />
            </asp:Panel>

        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:HiddenField runat="server" ID="pageLoadAlert" Value="" />
 
</asp:Content>
