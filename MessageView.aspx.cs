﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;


namespace HealthDemo {
    public partial class MessageView : System.Web.UI.Page {
        public string PATSELECT {
            get { return CommonCode.BlankIfNull(Session["HDExpandLog"]); }
            set { Session["HDExpandLog"] = CommonCode.NullIfBlank(value); }
        }

        protected void Page_Load(object sender, EventArgs e) {        }

        protected string[] BlackList = new string[10];
        protected List<string> NodeContentsList = new List<string>();
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e) {
            GridViewRow row = GridView1.SelectedRow;
            if (row != null) {
                BlackList = new string[] { "MRN", "GIVENNAME", "FAMILYNAME", "ADDRESS", "SEX", "MIDDLENAMES", "", "", "", ""};

                PATSELECT = GetCellString(row, 1);
                TreeNode root = new TreeNode(PATSELECT);

                String Qry = "SELECT ml.[msgId], a.ptmrn,r.[refText],[msgContent],[msgCreated],[msgDttm] ";
                Qry += "FROM [NSWHealthDemo].[dbo].[tblMessageLog] as ml ";
                Qry += "JOIN (SELECT ml.[msgId], mfv.ptmrn FROM [NSWHealthDemo].[dbo].[tblMessageLog] as ml join tblmessagefieldvalue as mfv on ml.msgId = mfv.maid group by msgId, ptmrn) as a on ml.msgId = a.msgId ";
                Qry += "LEFT OUTER JOIN tblReference as r on ml.msgTemplateId = r.refotherid WHERE [ptMrn] = " + PATSELECT;                                                
                DataTable m = ExecuteSql(Qry);

                foreach (DataRow r in m.Rows) {
                    try {
                        string maId = r["msgId"].ToString();
                        string msgId = r["msgId"].ToString();
                        TreeNode n = new TreeNode(r["refText"].ToString() + " : " + msgId + "   - Created: " + r["msgCreated"].ToString() + "   - DateTime: " + r["msgDttm"].ToString(), msgId);
                        DataTable p = ExecuteSql("SELECT * FROM [tblMessageFieldValue] WHERE [ptMrn] = " + PATSELECT + " AND [maId] = " + maId);

                        //DataTable p = ExecuteSql("SELECT [field],[parentField],COUNT(*)  FROM [NSWHealthDemo].[dbo].[tblMessageFieldValue]  where ptMrn = 1045623 and maId = 48  group by field, parentField ");
                        NodeContentsList.Clear();
                        n = DiveNode(p, n, "new","");
                        n.CollapseAll();
                        root.ChildNodes.Add(n);

                    } catch (Exception ex) {
                        string kk = "lsi";
                    }
                }
                
                TreeView1.Nodes.Clear();
                TreeView1.Nodes.Add(root);
                TreeView1.DataBind();
            }
        }
        
        protected TreeNode DiveNode(DataTable p, TreeNode pn, String pId, String fieldGroup) {
            foreach (DataRow r in p.Rows) {
                try {
                    String ParentField = r["parentField"].ToString();
                    String Field = r["field"].ToString();
                    String Value = r["value"].ToString();
                    String MfId = r["mfId"].ToString();

                    if (ParentField == "" && "new" == pId) {
                            pn.Value = r["mfId"].ToString();
                            pId = pn.Value;
                        }
                    if (ParentField == pId && ! NodeContentsList.Contains(Field.ToUpper()) && (fieldGroup == "" || fieldGroup == Field.ToUpper())) {
                            if (!BlackList.Contains(r["field"].ToString().ToUpper())) {
                                string Qry = "SELECT [field],[parentField],COUNT(*) as COUNT FROM [NSWHealthDemo].[dbo].[tblMessageFieldValue] ";
                                Qry += "WHERE ptMrn = " + PATSELECT + " AND [parentField] = " + pId + " AND field = '" + Field + "' ";
                                Qry += "GROUP BY field, parentField";
                                DataTable g = ExecuteSql(Qry);
                                foreach (DataRow gr in g.Rows) {
                                    try {
                                        int Count = (int)gr["COUNT"];
                                        if (Count > 1 && fieldGroup == "") {
                                                TreeNode n = new TreeNode(Field.ToUpper(), pId);
                                                n = DiveNode(p, n, n.Value, n.Text );
                                                pn.ChildNodes.Add(n);
                                                NodeContentsList.Add(n.Text);
                                            } else {
                                                TreeNode n = new TreeNode(appendPadRight(Field.ToUpper(),60) + " : " + MfId);
                                                n = DiveNode(p, n, n.Value, "");
                                                pn.ChildNodes.Add(n);
                                            }
                                        } catch (Exception ex) {
                                            string kkk = "lsi";
                                        }
                                    }
                            }
                        }
                    } catch (Exception ex) {
                        string kk = "lsi";
                    }
                }
            return pn;
        }

        private string appendPadRight(string s, int i) {
            int c = 0;
            do {
                if (s.Length < c) { s += " "; }
                c += 1;
            } while (c<i);

            return s;
        }

        private string GetCellString(GridViewRow row, int cellNo) {
            string result = row.Cells[cellNo].Text;
            return result.Replace("&nbsp;", "");
        }

        private DataTable ExecuteSql(string sql) {
            DataSet ODs = new DataSet();
            try {
                string Sqlcon = ConfigurationManager.AppSettings["HDConnection"].ToString();
                SqlConnection ocon = new SqlConnection(Sqlcon);
                ocon.Open();
                SqlDataAdapter oDA = new SqlDataAdapter(sql, ocon);
                oDA.Fill(ODs);
                ocon.Close();
            } catch (Exception ex) {
                CommonCode.WriteLog("sql=\"" + sql + "\"\r\n\r\nMessage: " + ex.Message + "\r\n\r\nStack Trace: " + ex.StackTrace, "ExecuteSql");
            }
            if (ODs.Tables.Count > 0)
                return ODs.Tables[0];
            else
                return null;
        }
    }
}