﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MessageView.aspx.cs" Inherits="HealthDemo.MessageView" %>

<%@ Register assembly="System.Web.Entity, Version=3.5.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" namespace="System.Web.UI.WebControls" tagprefix="asp" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title></title>
    <meta name="viewport" content="width=device-width; initial-scale=1.0" />
    <meta name="apple-touch-fullscreen" content="yes" />
    <meta name="apple-mobile-web-app-capable" content="no" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <link href="css/normalize.css" rel="stylesheet" type="text/css" />
    <link href="css/eHealthflow.css" rel="stylesheet" type="text/css" />
    <link href="css/obj_button.css" rel="stylesheet" />
    <link href="/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <link runat="server" href="css/Default.css" rel="stylesheet" type="text/css" id="style_default" visible="false" />
    <link rel="apple-touch-icon-precomposed" sizes="114×114" href="images/apple-touch-icon-114×114-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72×72" href="images/apple-touch-icon-72×72-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" href="images/touch-icon-iphone-precomposed.png" />
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="Scripts/modernizr-2.6.2.js"></script>
</head>
<body id="minwidth-body">
        <form id="form1" runat="server">
            
            <asp:ScriptManager runat="server"></asp:ScriptManager>

                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="ptMrn,ptFamilyName" DataSourceID="PatViewsDS" EnableModelValidation="True" OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
                            <Columns>
                                <asp:CommandField ShowSelectButton="True" />
                                <asp:BoundField DataField="ptMrn" HeaderText="ptMrn" ReadOnly="True" SortExpression="ptMrn" />
                                <asp:BoundField DataField="ptGivenName" HeaderText="ptGivenName" SortExpression="ptGivenName" />
                                <asp:BoundField DataField="ptFamilyName" HeaderText="ptFamilyName" SortExpression="ptFamilyName" ReadOnly="True" />
                                <asp:BoundField DataField="ptDOB" HeaderText="ptDOB" SortExpression="ptDOB" />
                                <asp:BoundField DataField="ptGender" HeaderText="ptGender" SortExpression="ptGender" />
                                <asp:BoundField DataField="ptAddressLine1" HeaderText="ptAddressLine1" SortExpression="ptAddressLine1" />
                                <asp:BoundField DataField="ptAddressLine2" HeaderText="ptAddressLine2" SortExpression="ptAddressLine2" />
                                <asp:BoundField DataField="ptSuburb" HeaderText="ptSuburb" SortExpression="ptSuburb" />
                                <asp:BoundField DataField="ptPostCode" HeaderText="ptPostCode" SortExpression="ptPostCode" />
                                <asp:BoundField DataField="MessageCount" HeaderText="MessageCount" SortExpression="MessageCount" />
                            </Columns>
                            <SelectedRowStyle BackColor="#3399FF" />
                        </asp:GridView>

                        <asp:EntityDataSource ID="PatViewsDS" runat="server" ConnectionString="name=NSWHealthDemoEntities1" DefaultContainerName="NSWHealthDemoEntities1" EntitySetName="V_Patient" EntityTypeFilter="V_Patient">
            </asp:EntityDataSource>
            <asp:ObjectDataSource ID="PatViewsDataSource" runat="server"></asp:ObjectDataSource>

                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <hr />
                                <asp:TreeView ID="TreeView1" runat="server" ShowLines="True" SkipLinkText="">
                                </asp:TreeView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
    <script src="js/helper.js"></script>
    <!-- CSS3 Media Queries -->
    <script src="js/respond.min.js"></script>
    <!-- iOS scale bug fix -->
    <script>
        MBP.scaleFix();
    </script>
    <noscript>JavaScript is unavailable or disabled; so you are probably going to miss out on a few things. Everything should still work, but with a little less pzazz!
        <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" DataKeyNames="mfId" DataSourceID="NSWMessageSQL" EnableModelValidation="True">
            <Columns>
                <asp:BoundField DataField="mfId" HeaderText="mfId" InsertVisible="False" ReadOnly="True" SortExpression="mfId" />
                <asp:BoundField DataField="maId" HeaderText="maId" SortExpression="maId" />
                <asp:BoundField DataField="ptMrn" HeaderText="ptMrn" SortExpression="ptMrn" Visible="False" />
                <asp:BoundField DataField="field" HeaderText="field" SortExpression="field" />
                <asp:BoundField DataField="value" HeaderText="value" SortExpression="value" />
                <asp:BoundField DataField="parentField" HeaderText="parentField" SortExpression="parentField" />
            </Columns>
        </asp:GridView>
                        <asp:SqlDataSource ID="NSWMessageSQL" runat="server" ConnectionString="<%$ ConnectionStrings:NSWHealthDemoConnectionString %>" SelectCommand="SELECT * FROM [tblMessageFieldValue] WHERE ([ptMrn] = @ptMrn2)">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="GridView1" Name="ptMrn2" PropertyName="SelectedValue" Type="String" />
                            </SelectParameters>
                        </asp:SqlDataSource>
            
                        <asp:EntityDataSource ID="NSWPatientEnt" runat="server" ConnectionString="name=NSWHealthDemoEntities1" DefaultContainerName="NSWHealthDemoEntities1" EntitySetName="tblPatients" EntityTypeFilter="tblPatient">
                        </asp:EntityDataSource>
            
                        </noscript>
        </form>
    </body>
</html>